summary: Assert a user process can't write outside it's address space.
description: |
    Assert a user process can't write outside it's address space.
    The test process allocates a PAGE_SIZE shared memory segment. Then it creates
    a child process that writes bytes to the whole segment + 1. The parent
    asserts that the child process is killed by SIGSEGV.

    Test input:
        Two versions of the program shm-overlow: one for SysV shm and the other
        (shm-overflow-posix) for POSIX shm. Input details:
        - shm-overflow.c, source
        - /tmp/shm-overflow
    Expected results:
        If the write operation outside the segment's address space fails as expected,
        you should expect the following output:
        [   PASS   ] :: Command 'gcc -o /tmp/shm-overflow -D_GNU_SOURCE shm-overflow.c' (Expected 0, got 0)
        Child process terminated with exit status 0x008B.
        Received SIGSEGV as expected.
        [   PASS   ] :: Command '/tmp/shm-overflow' (Expected 0, got 0)

        [   PASS   ] :: Command 'gcc -o /tmp/shm-overflow-posix -DUSE_POSIX_INTERFACE -D_GNU_SOURCE shm-overflow.c' (Expected 0, got 0)
        Child process terminated with exit status 0x008B.
        Received SIGSEGV as expected.
        [   PASS   ] :: Command '/tmp/shm-overflow-posix' (Expected 0, got 0)
    Results location:
        output.txt
contact: Pablo Ridolfi <pridolfi@redhat.com>
test: |
    if [ -n "${FFI_QM_SCENARIO}" ]; then
        podman exec --env TEST_DIR="$(pwd)" --env BEAKERLIB_DIR="$BEAKERLIB_DIR" \
          -t qm sh -c 'cd "$TEST_DIR"; bash runtest.sh'
    else
        bash ./runtest.sh
    fi
framework: beakerlib
require:
  - beakerlib
  - gcc
  - glibc-devel
extra-summary: memory/mmra/shm-overflow
extra-task: memory/mmra/shm-overflow
id: 076fec2c-f51f-4226-9fea-7bd7cf7931ac
enabled: true
duration: 10m
