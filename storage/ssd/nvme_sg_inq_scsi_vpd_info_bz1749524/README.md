# storage/ssd/nvme_sg_inq_scsi_vpd_info_bz1749524

Storage: nvme sg_inq for SCSI VPD information 

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
#### You need run test_devs_setup to define TEST_DEVS
```bash
bash ./runtest.sh
```
