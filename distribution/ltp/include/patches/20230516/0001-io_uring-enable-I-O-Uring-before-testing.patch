From c173a36291b33bea7097c88c8621d553a9b613cb Mon Sep 17 00:00:00 2001
From: Li Wang <liwang@redhat.com>
Date: Tue, 8 Aug 2023 20:44:45 +0800
Subject: [PATCH] io_uring: enable I/O Uring before testing

Given that the upstream kernel is going to introduce io_uring_disabled
knob which disables the creation of new io_uring instances system-wide.

The new sysctl is designed to let a user with root on the machine
enable and disable io_uring system-wide at runtime without requiring
a kernel recompilation or a reboot.

See: https://patchwork.kernel.org/project/io-uring/patch/20230630151003.3622786-2-matteorizzo@google.com/

After that, if test with kernel disabling io_uring, LTP/io_uring complains:

  io_uring01.c:82: TFAIL: io_uring_setup() failed: EPERM (1)
  io_uring02.c:213: TBROK: io_uring_setup() failed: EPERM (1)

Reported-by: Jeff Moyer <jmoyer@redhat.com>
Signed-off-by: Li Wang <liwang@redhat.com>
Reviewed-by: Jeff Moyer <jmoyer@redhat.com>
---
 testcases/kernel/syscalls/io_uring/io_uring01.c | 5 +++++
 testcases/kernel/syscalls/io_uring/io_uring02.c | 5 +++++
 2 files changed, 10 insertions(+)

diff --git a/testcases/kernel/syscalls/io_uring/io_uring01.c b/testcases/kernel/syscalls/io_uring/io_uring01.c
index 70151bb85..ab1ec00d6 100644
--- a/testcases/kernel/syscalls/io_uring/io_uring01.c
+++ b/testcases/kernel/syscalls/io_uring/io_uring01.c
@@ -264,5 +264,10 @@ static struct tst_test test = {
 	.bufs = (struct tst_buffers []) {
 		{&iov, .iov_sizes = (int[]){BLOCK_SZ, -1}},
 		{}
+	},
+	.save_restore = (const struct tst_path_val[]) {
+		{"/proc/sys/kernel/io_uring_disabled", "0",
+			TST_SR_SKIP_MISSING | TST_SR_TCONF_RO},
+		{}
 	}
 };
diff --git a/testcases/kernel/syscalls/io_uring/io_uring02.c b/testcases/kernel/syscalls/io_uring/io_uring02.c
index c5c770074..c9d4bbcb1 100644
--- a/testcases/kernel/syscalls/io_uring/io_uring02.c
+++ b/testcases/kernel/syscalls/io_uring/io_uring02.c
@@ -255,6 +255,11 @@ static struct tst_test test = {
 		TST_CAP(TST_CAP_REQ, CAP_SYS_CHROOT),
 		{}
 	},
+	.save_restore = (const struct tst_path_val[]) {
+		{"/proc/sys/kernel/io_uring_disabled", "0",
+			TST_SR_SKIP_MISSING | TST_SR_TCONF_RO},
+		{}
+	},
 	.tags = (const struct tst_tag[]) {
 		{"linux-git", "9392a27d88b9"},
 		{"linux-git", "ff002b30181d"},
-- 
2.40.1

